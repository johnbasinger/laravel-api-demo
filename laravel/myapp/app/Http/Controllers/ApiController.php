<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\members;

class ApiController extends Controller {

    public function userUpdate(Request $req) {
//        $user = New App\Models\User;
        $member = New members();
        $upData['email'] = $req->email;
        $upData['name'] = $req->name;
        $id = $req->id;
//        print_r($id); die;
        $update = $member->updateMember($upData, $id);
//        if ($update != ''):
        $resp ['error'] = '';
        $resp ['error_msg'] = 'User Updated succesfully';
//        else:
//            $resp ['error'] = '1';
//            $resp ['error_msg'] = 'not Updated';
//        endif;
        return response()->json($resp);
    }

    public function userLogin(Request $req) {
//        $user = New App\Models\User;
        $member = New members();
        $email = $req->email;
        $pass = $req->password;
        $memberCheck = $member->searchMemberByEmail($email);
        $memberCheck1 = $member->searchMemberByEmailPass($email, $pass)[0];
// print_r($memberCheck1); die;
        if (count($memberCheck) && isset($memberCheck1->name)):
            $resp ['error'] = '';
            $resp ['error_msg'] = 'User Loggedin succesfully';
            $resp['name'] = $memberCheck1->name;
            $resp['email'] = $memberCheck1->email;
            $resp['id'] = $memberCheck1->id;
        elseif (count($memberCheck)):
            $resp ['error'] = '1';
            $resp ['error_msg'] = 'Password Error';
        else:
            $resp ['error'] = '2';
            $resp ['error_msg'] = 'Invalid Login';
        endif;
        return response()->json($resp);
    }

    public function userSignup(Request $req) {
//        $user = New App\Models\User;
        $member = New members();
        $email = $req->email;
        $memberCheck = $member->searchMemberByEmail($email);
//        print_r($memberCheck); die;
        if (count($memberCheck)):
            $resp ['error'] = 1;
            $resp ['error_msg'] = 'User already registered';
        else:
            $insData['name'] = $req->name;
            $insData['email'] = $req->email;
            $insData['password'] = md5($req->password);
            $insData['status'] = '1';
            $insData['created_at'] = date('Y-m-d H:i:s');
            $insData['updated_at'] = date('Y-m-d H:i:s');
            $insId = $member->insertInto('members', $insData);
            if ($insId):
                $resp ['id'] = $insId;
                $resp ['error'] = '';
                $resp ['error_msg'] = 'User registered successfully';
            else:
                $resp ['error'] = 1;
                $resp ['error_msg'] = 'User registration failed';
            endif;
        endif;
        return response()->json($resp);
    }

}
