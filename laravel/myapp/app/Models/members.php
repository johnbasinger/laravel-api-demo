<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class members extends Model {

    use HasFactory;

    public function searchMemberByEmail($email) {
        $res = DB::table('members')->where('email', '=', $email)->get();
        return $res;
    }

    public function searchMemberByEmailPass($email, $pass) {
        $pass = md5($pass);
        $res = DB::table('members')->where('email', '=', $email)->where('password', '=', $pass)->get();
        return $res;
    }

    public function insertInto($table, $data) {
        $id = DB::table($table)->insertGetId($data);
        return $id;
    }

    public function updateMember($data, $id) {
        $id = DB::table('members')->where('id', $id)->update($data);
        return $id;
    }

}
