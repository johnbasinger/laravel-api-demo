<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    echo "Hi";
//    die;
//    return $request->user();
//});

Route::post('usersignup', 'App\Http\Controllers\ApiController@userSignup');
Route::post('userlogin', 'App\Http\Controllers\ApiController@userLogin');
Route::post('userupdate', 'App\Http\Controllers\ApiController@userUpdate');
