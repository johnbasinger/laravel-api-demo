function validateUpdate() {
    var regform = $('#updateform');
    regform.parsley().validate();
    if (regform.parsley().isValid()) {
        updateUser();
    }
}
function validateLogin() {
    var regform = $('#logform');
    regform.parsley().validate();
    if (regform.parsley().isValid()) {
        loginUser();
    }
}
function validateRegister() {
    var regform = $('#regform');
    regform.parsley().validate();
    if (regform.parsley().isValid()) {
        regitserUser();
    }
}
function updateUser() {
    var e = $('#dash_email').val();
    var n = $('#dash_name').val();
    var i = sessionStorage.getItem("id");
    alert
    $.ajax({
        type: "POST",
        url: apiUrl + 'userupdate',
        data: {email: e, name: n, id: i},
        dataType: 'json',
        success: function (res) {
            console.log(res);
            if (res.error == '') {
                sessionStorage.setItem("name", n);
                sessionStorage.setItem("email", e);
                sessionStorage.setItem("id", i);
                checkLogin();
//                closeForm('persDet');
                $('#alert').addClass('alert-success');
                $('.alert').alert()
            } else {
                $('#alert').addClass('alert-danger');
                $('.alert').alert()
                checkLogin();
            }
            $('#alertCont').html(res.error_msg);
        }
    });
}
function loginUser() {
    var e = $('#login_email').val();
    var p = $('#login_password').val();
    alert
    $.ajax({
        type: "POST",
        url: apiUrl + 'userlogin',
        data: {email: e, password: p},
        dataType: 'json',
        success: function (res) {
            console.log(res);
            if (res.error == '') {
                sessionStorage.setItem("name", res.name);
                sessionStorage.setItem("email", res.email);
                sessionStorage.setItem("id", res.id);
                checkLogin();
                $('#alert').addClass('alert-success');
                $('.alert').alert()
//                closeForm('persDet');
            } else {
                sessionStorage.clear();
                $('#alert').addClass('alert-danger');
                $('.alert').alert()
                checkLogin();
            }
            $('#alertCont').html(res.error_msg);
        }
    });
}
function logoutUser() {
    sessionStorage.clear();
    checkLogin();
}
function regitserUser() {
    var e = $('#signup_email').val();
    var p = $('#signup_password').val();
    var n = $('#signup_name').val();
    alert
    $.ajax({
        type: "POST",
        url: apiUrl + 'usersignup',
        data: {email: e, password: p, name: n},
        dataType: 'json',
        success: function (res) {
            console.log(res);
            if (res.error == '') {
                sessionStorage.setItem("name", n);
                sessionStorage.setItem("email", e);
                sessionStorage.setItem("id", i);
                checkLogin();
//               $('#alert').addClass('alert-success');
                $('.alert').alert()
//                closeForm('persDet');
            } else {
                sessionStorage.clear();
                $('#alert').addClass('alert-danger');
                $('.alert').alert()
                checkLogin();
            }
            $('#alertCont').html(res.error_msg);
        }
    });
}

$(document).ready(function () {
//    sessionStorage.clear();
    checkLogin();
});

function checkLogin() {
    if (sessionStorage.getItem("name") != null) {
        var name = sessionStorage.getItem("name");
        var em = sessionStorage.getItem("email");
        $('#loginContainer').addClass('d-none');
        $('#profileForm').removeClass('d-none');
        $('#dash_name').val(name);
        $('#dash_email').val(em);
    } else {
        $('#profileForm').addClass('d-none');
        $('#loginContainer').removeClass('d-none');
    }
}